﻿using BydlocodeLoader.Model;
using System;
using System.Windows.Forms;

namespace BydlocodeLoader
{
    public partial class FirmwareManager : Form
    {
        string serverUri = @"http://localhost:5001/";
        string GetApplicationList = "api/Update";
        HostClient hostClient;

        public FirmwareManager()
        {
            InitializeComponent();
            hostClient = HostClient.getInstance();
        }

        private void FirmwareManager_Load(object sender, EventArgs e)
        {
            Package[] packages = hostClient.GetRequest<Package[]>(serverUri + GetApplicationList);
            if (packages == null)
            {
                MessageBox.Show("Firmware services are currently unavailable");
                this.Close();
            }
            else
            {
                foreach (Package package in packages)
                {
                    // new ListViewItem()
                    //   treeListView1.Items.Add(package.PackageName);
                }
            }
           
        }
    }
}
