﻿using IntelHexFormatReader;
using IntelHexFormatReader.Model;
using SerialPortLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TCP_channel;

namespace BydlocodeLoader
{
    public partial class MainForm : Form
    {
        FirmwareManager firmwareManager;
        HexFileReader HexFileReader;

       // string startFlashCmd = "BLST";
        byte[] startFlashCmdBytes = new byte[] { (byte)'B', (byte)'L', (byte)'S', (byte)'T' };
        //string writePageCmd = "BLPG";
        byte[] writePageCmdBytes = new byte[] { (byte)'B', (byte)'L', (byte)'P', (byte)'G' };
        //string exitFlashCmd = "BLXT";
        byte[] exitFlashCmdBytes = new byte[] { (byte)'B', (byte)'L', (byte)'X', (byte)'T' };

        byte[] exitFlashCmdConfirmedBytes = new byte[] { (byte)'b', (byte)'l', (byte)'x', (byte)'t' };

        Client client;
        SerialPortInput serialPort; 

        int pageSize;
        byte pageCount;
        byte[] hardwareInfo = new byte[14];
        public MainForm()
        {
            InitializeComponent();
        }
        
        private void selectFile_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = openFileDialog1.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                fileNameTxt.Text = openFileDialog1.FileName;
            }
        }

        Thread LogThread;
        static ConcurrentQueue<byte> itemsToProc = new ConcurrentQueue<byte>();
        private static ManualResetEvent mre = new ManualResetEvent(false);
        List<int> cnt = new List<int>();
        void StartReader()
        {
            mre = new ManualResetEvent(false);
            if (LogThread != null) {
                LogThread.Abort();
                while (LogThread.IsAlive) { Thread.Sleep(500); }
            }
            LogThread = new Thread(ThreadMethod);
            LogThread.IsBackground = true;
            LogThread.Start();
        }
        void StopReader()
        {
            mre.Set();
            LogThread.Abort();
        }
        void ThreadMethod()
        {
            List<byte> bytes;
            bytes = new List<byte>();
            byte itemToWrite;
            while (true)
            {
                //mre.WaitOne();
                while (itemsToProc.TryDequeue(out itemToWrite))
                {
                    if (cnt.Count > 0)
                    {
                        if (bytes.Count < cnt[0])
                        {
                            bytes.Add(itemToWrite);
                        }
                        if (bytes.Count == cnt[0])
                        {
                            if (itemToWrite == 103 && bytes[2] == 112 && bytes[bytes.Count-2] != 103)// g - page successfuly uploaded
                            {
                                cnt[0]++;
                                continue;
                            }

                            Invoke(new Action(() =>
                            {
                                ProcessData(bytes.ToArray());
                            }));
                            cnt.RemoveAt(0);
                            bytes.Clear();
                        }
                    }
                }
                mre.Reset();
            }
        }
        List<List<byte>> splitedFile = new List<List<byte>>();
        List<byte> checkSums = new List<byte>();
        void ReadFileFirmware()
        {
            progressBar1.Value = 0;
            //  byte[] fileBytes = StreamHelper.ReadToEnd(openFileDialog1.OpenFile());
            HexFileReader = new HexFileReader(openFileDialog1.FileName, 32 * 1024);
            MemoryBlock memoryBlock = HexFileReader.Parse();

            MemoryCell[] cells = memoryBlock.Cells.Where(c => c.Modified == true).ToArray();
            Log("File lenght = " + cells.Length + " bytes (" + ((float)cells.Length / (float)(pageCount * (pageSize * 2))).ToString("00.00%") + ")");

            int currentPage = 0;
            splitedFile.Clear();
            
            splitedFile.Add(new List<byte>());
            for (int n = 0; n < cells.Length; n++)
            {
                if (splitedFile[currentPage].Count < pageSize * 2)
                {
                    splitedFile[currentPage].Add(cells[n].Value);
                }
                else
                {
                    splitedFile.Add(new List<byte>());
                    currentPage++;
                    splitedFile[currentPage].Add(cells[n].Value);
                }
            }
            for (int k=splitedFile.Last().Count;k< pageSize * 2; k++)
            {
                splitedFile.Last().Add(255);
            }
            Log("Pages to load = " + splitedFile.Count);
            progressBar1.Maximum = splitedFile.Count;
            checkSums.Clear();
            for (int n = 0; n < splitedFile.Count; n++)
            {
                byte cc = StreamHelper.Crc8.ComputeChecksum(splitedFile[n].ToArray());
                checkSums.Add(cc);
                cnt.Add(4);
            }
            StartSendFirmwarePage();
        }
        byte currentPageToSend = 0;
        bool sendByteByByte = false;
        void StartSendFirmwarePage()
        {
            if (sendByteByByte)
                foreach (byte b in writePageCmdBytes)
                    SendData(b);
            else
                SendData(writePageCmdBytes);

            SendData(currentPageToSend);

            if (sendByteByByte)
                foreach (byte b in splitedFile[currentPageToSend].ToArray())
                    SendData(b);
            else
                SendData(splitedFile[currentPageToSend].ToArray());

            SendData(checkSums[currentPageToSend]);
        }
        // private static ManualResetEvent sendNext = new ManualResetEvent(false);
        void ProcessData(byte[] data)
        {
            //foreach (byte b in data)
            //    Log(b);
            //Console.WriteLine(" Received " + data.Length);
            switch (data.Length)
            {
                case 20:
                    pageSize = data[4];
                    pageCount = data[5];
                    for (int i = 6; i < 20; i++)
                    {
                        hardwareInfo[i - 6] = data[i];
                    }
                    Log("Page size must be = " + pageSize + "( *2)");
                    Log("Available pages to write = " + pageCount);
                    Log("HardwareInfo = ");
                    Log(hardwareInfo);
                    if (HIDcheck.Checked)
                    {
                        string hid = Encoding.ASCII.GetString(hardwareInfo).ToLower();
                        if (hid == HidTxt.Text.Trim().ToLower())
                        {
                            Log("Совпедение HID-ов. Прошивка остановлена");
                            MessageBox.Show("Совпедение HID-ов. Прошивка остановлена", "Ошибка");
                            Application.Exit();
                        }
                    }
                    Log("Memory available = " + pageCount * (pageSize * 2) + " bytes");
                    ReadFileFirmware();
                    break;

                case 4:
                    if (data[0] == 98 && data[1] == 108 && data[2] == 101 && data[3] == 114) // error
                    {
                        Log("Не удалось записать " + currentPageToSend + " страницу");
                        Repeat();
                    }
                    else
                         if (data[0] == exitFlashCmdConfirmedBytes[0] && data[1] == exitFlashCmdConfirmedBytes[1] &&
                         data[2] == exitFlashCmdConfirmedBytes[2] && data[3] == exitFlashCmdConfirmedBytes[3])
                    {
                        Log("Прошивка успешно записана.");
                        Thread.Sleep(2000);
                        progressBar1.Value = progressBar1.Maximum;
                        if (radioButton1.Checked)
                        client.Disconnect();
                        if (radioButton2.Checked)
                            serialPort.Disconnect();
                        StopReader();
                        flashBtn.Enabled = true;
                    }
                    break;

                case 5:
                    if (data[0] == 98 && data[1] == 108 && data[2] == 112 && data[3] == 103)
                    {
                        if (data[4] == currentPageToSend)
                        {
                            Log("Успешно записал " + currentPageToSend + " страницу");

                            if (currentPageToSend == splitedFile.Count - 1)
                            {
                                Log("Все страницы записаны");
                                currentPageToSend = 0;
                                cnt.Add(4);
                                if (sendByteByByte)
                                    foreach (byte b in exitFlashCmdBytes)
                                        SendData(b);
                                else
                                    SendData(exitFlashCmdBytes);
                            }
                            else
                            {
                                currentPageToSend++;
                                Invoke(new Action(() =>
                                {
                                    progressBar1.Value = currentPageToSend;
                                }));
                               // Thread.Sleep(100);
                                StartSendFirmwarePage();
                            }
                        }
                    }
                    break;
            }
        }
        int repVal = 0;
        int repeatCount = 1;
        void Repeat()
        {
            if (repVal != currentPageToSend)
            {
                repVal = currentPageToSend;
                repeatCount = 1;
            }
            if (repeatCount <= 20)
            {
                Log("Попытка #" + repeatCount + " записать " + currentPageToSend + " страницу");
                StartSendFirmwarePage();
                repeatCount++;
            }
            else
            {
                flashBtn.Enabled = true;
                Log("Прошивка не удалась :(");
            }
        }
        void StartWrite()
        {
            if (radioButton1.Checked) // tcp
            {
                if (client.socketConnection == null)
                {
                    Log("Подключение");
                    client = new Client();
                    client.allowReconnect = false;
                    client.dataReceived += Client_dataReceived;
                    client.Start(ipAddressTxt.Text.Trim(), int.Parse(portTxt.Text.Trim()));
                    DateTime dateTimeStart = DateTime.Now;
                    int cntr = 30;
                    Log(".");
                    while (client.socketConnection == null)
                    {
                        if ((DateTime.Now - dateTimeStart).TotalSeconds > 30)
                        {
                            flashBtn.Text = "Flash";
                            Log("Нет соединения");
                            flashBtn.Enabled = true;
                            return;
                        }
                        flashBtn.Text = "Flash (" + cntr + ")";
                        flashBtn.Invalidate();
                        flashBtn.Update();
                        flashBtn.Refresh();
                        LogWONL(".");
                        logTxt.Invalidate();
                        logTxt.Update();
                        logTxt.Refresh();
                        Application.DoEvents();
                        Thread.Sleep(1000);
                        cntr--;
                    }
                    flashBtn.Text = "Flash";
                    ConnectionChecker.Start();
                }
            }
            if (radioButton2.Checked)
            {
                Log("Подключение");
                serialPort = new SerialPortInput();
                serialPort.MessageReceived += SerialPort_MessageReceived;
                serialPort.SetPort(portCombo.SelectedItem.ToString().Trim(), int.Parse(portSpeed.Text.Trim()));
                serialPort.Connect();
                while (!serialPort.IsConnected) { }
            }
            if (sendByteByByte)
                foreach (byte b in startFlashCmdBytes)
                    SendData(b);
            else
                SendData(startFlashCmdBytes);

        }

        private void SerialPort_MessageReceived(object sender, MessageReceivedEventArgs args)
        {
            if (debugCheck.Checked)
            {
                Log("==================END===================");
                Log(args.Data);
                Log("---------------- BYTES: ----------------");
                Log(Encoding.ASCII.GetString(args.Data));
                Log("=========== PACKET RECEIVED ============");
            }
            addBytes(args.Data);
            mre.Set();
        }

        private void Client_dataReceived(byte[] data)
        {
            if (debugCheck.Checked)
            {
                Log("==================END===================");
                Log(data);
                Log("---------------- BYTES: ----------------");
                Log(Encoding.ASCII.GetString(data));
                Log("=========== PACKET RECEIVED ============");
            }
            addBytes(data);
            mre.Set();
            // foreach (byte b in data) Log(b);
            //  logTxt.Text += Environment.NewLine;
        }
        public void addBytes(byte[] bytes)
        {
            foreach (byte b in bytes)
                itemsToProc.Enqueue(b);
        }
        private void flashBtn_Click(object sender, EventArgs e)
        {
            if (portCombo.SelectedItem.ToString().Trim().Length <= 3 && radioButton2.Checked)
            {
                MessageBox.Show("А порт то забыл");
                return;
            }
            repVal = 0;
            repeatCount = 1;
            cnt.Clear();
            itemsToProc = new ConcurrentQueue<byte>();
            cnt.Add(20);
            StartReader();
            if (File.Exists(fileNameTxt.Text))
            {
                Properties.Settings.Default.ipAddress = ipAddressTxt.Text.Trim();
                Properties.Settings.Default.filePath = fileNameTxt.Text;
                Properties.Settings.Default.port = int.Parse(portTxt.Text);
                Properties.Settings.Default.filterChecked = HIDcheck.Checked;
                Properties.Settings.Default.filterText = HidTxt.Text;
                Properties.Settings.Default.comPort = portCombo.SelectedItem.ToString();
                Properties.Settings.Default.portSpeed = portSpeed.SelectedIndex;
                Properties.Settings.Default.Save();
                flashBtn.Enabled = false;
                StartWrite();
            }
            else
            {
                MessageBox.Show("Файл не найден");
            }
        }

        void Log(byte[] bytes)
        {
            logTxt.Text = Environment.NewLine + logTxt.Text;
            Invoke(new Action(() =>
            {
                foreach(byte b in bytes)
                logTxt.Text = b + " " + logTxt.Text;
            }));
            logTxt.Text = DateTime.Now.ToString("HH:mm:ss:fff") + ": " + logTxt.Text;
        }
        void Log(string b)
        {
            Invoke(new Action(() =>
            {
                logTxt.Text = DateTime.Now.ToString("HH:mm:ss:fff")+": "+b + Environment.NewLine + logTxt.Text;
            }));

        }
        void LogWONL(string b)
        {
            Invoke(new Action(() =>
            {
                logTxt.Text = b +logTxt.Text;
            }));
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            this.Text = string.Format("Bydlocode flasher v{0}", version);
            string[] ports = SerialPort.GetPortNames();
            portCombo.Items.Clear();
            foreach(string port in ports)
            {
                portCombo.Items.Add(port);
            }
            if (Properties.Settings.Default.comPort != "")
            {
                if (ports.Contains(Properties.Settings.Default.comPort))
                {
                    portCombo.SelectedIndex = portCombo.Items.IndexOf(Properties.Settings.Default.comPort);
                }
            }
            if (Properties.Settings.Default.ipAddress != "")
            {
                ipAddressTxt.Text = Properties.Settings.Default.ipAddress;
            }
            if (Properties.Settings.Default.filePath != "")
            {
                fileNameTxt.Text = Properties.Settings.Default.filePath;
                openFileDialog1.FileName = fileNameTxt.Text;
            }
            if (Properties.Settings.Default.port != 0)
                portTxt.Text = Properties.Settings.Default.port.ToString();
                HIDcheck.Checked = Properties.Settings.Default.filterChecked;
            if (Properties.Settings.Default.filterText!="")
                HidTxt.Text=Properties.Settings.Default.filterText;

            portSpeed.SelectedIndex = Properties.Settings.Default.portSpeed;
        }
        
        public void Split<T>(T[] array, int index, out T[] first, out T[] second)
        {
            first = array.Take(index).ToArray();
            second = array.Skip(index).ToArray();
        }

        private void portTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (client.socketConnection.Connected)
            Repeat();
        }

        private void sendNextBtn_Click(object sender, EventArgs e)
        {
            if (client.socketConnection.Connected)
            {
                if (currentPageToSend == splitedFile.Count - 1)
                {
                    cnt.Add(4);
                    if (sendByteByByte)
                        foreach (byte b in exitFlashCmdBytes)
                            SendData(b);
                    else
                        SendData(exitFlashCmdBytes);
                }
                else
                {
                    currentPageToSend++;

                    Repeat();
                }
            }
        }

        void SendData(byte b)
        {
            if (debugCheck.Checked)
            {
                Log("==================END===================");
                Log(new byte[] { b });
                Log("---------------- BYTES: ----------------");
                Log(Encoding.ASCII.GetString(new byte[] { b }));
                Log("============= PACKET SENT ==============");
            }
            if (radioButton2.Checked)
                serialPort.SendMessage(new byte[] { b });
            if (radioButton1.Checked)
                client.SendMessage(b);
        }

        void SendData(byte[] data)
        {
            if (debugCheck.Checked)
            {
                Log("==================END===================");
                Log(data);
                Log("---------------- BYTES: ----------------");
                Log(Encoding.ASCII.GetString(data));
                Log("============= PACKET SENT ==============");
            }
            if (radioButton2.Checked)
            serialPort.SendMessage(data);
            if (radioButton1.Checked)
                client.SendMessage(data);
            Console.WriteLine(data.Length);
        }

        private void ConnectionChecker_Tick(object sender, EventArgs e)
        {
            if (!client.socketConnection.Connected)
            {
                Log("Сокет был отключен!");
                ConnectionChecker.Stop();
                flashBtn.Enabled = true;
            }
        }

        private void FirmwareManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (firmwareManager == null)
            {
                firmwareManager = new FirmwareManager();
                firmwareManager.Show();
            }
        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            Log("Подключение.");
            client = new Client();
            client.allowReconnect = false;
            client.dataReceived += Client_dataReceived;
            client.Start(ipAddressTxt.Text.Trim(), int.Parse(portTxt.Text.Trim()));
            DateTime dateTimeStart = DateTime.Now;
            int cntr = 30;
            while (client.socketConnection == null)
            {
                if ((DateTime.Now - dateTimeStart).TotalSeconds > 30)
                {
                    flashBtn.Text = "Flash";
                    Log("Нет соединения");
                    flashBtn.Enabled = true;
                    return;
                }
                LogWONL(".");
                logTxt.Invalidate();
                logTxt.Update();
                logTxt.Refresh();
                Application.DoEvents();
                Thread.Sleep(1000);
                cntr--;
            }
            flashBtn.Text = "Flash";
            if (client.socketConnection!=null)
            Log("Подключено");
            btnSendTxt.Enabled = true;
            ConnectionChecker.Start();
        }

        private void logClearBtn_Click(object sender, EventArgs e)
        {
            logTxt.Clear();
        }

        private void btnSendTxt_Click(object sender, EventArgs e)
        {
            SendData(Encoding.ASCII.GetBytes(textToSendTxt.Text));
        }
    }
}
