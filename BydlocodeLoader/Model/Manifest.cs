﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BydlocodeLoader.Model
{
    [DataContract]
    [Serializable]
    public class Manifest
    {
        [DataMember]
        public string PackageName { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Version { get; set; }
    }
    [DataContract]
    [Serializable]
    public class Package : Manifest
    {
        [DataMember]
        public string FileUri { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long Size { get; set; }
    }
    [DataContract]
    [Serializable]
    public class PackageCollection
    {
        public PackageCollection() { Packages = new List<Package>(); }
        [DataMember]
        public List<Package> Packages { get; set; }
    }
}
