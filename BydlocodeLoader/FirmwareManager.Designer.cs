﻿namespace BydlocodeLoader
{
    partial class FirmwareManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.treeListView1 = new BrightIdeasSoftware.TreeListView();
            this.nameC = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.versionC = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.createdC = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.sizeC = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // treeListView1
            // 
            this.treeListView1.AllColumns.Add(this.nameC);
            this.treeListView1.AllColumns.Add(this.versionC);
            this.treeListView1.AllColumns.Add(this.createdC);
            this.treeListView1.AllColumns.Add(this.sizeC);
            this.treeListView1.CellEditUseWholeCell = false;
            this.treeListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameC,
            this.versionC,
            this.createdC,
            this.sizeC});
            this.treeListView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeListView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.treeListView1.Location = new System.Drawing.Point(0, 66);
            this.treeListView1.Name = "treeListView1";
            this.treeListView1.ShowGroups = false;
            this.treeListView1.Size = new System.Drawing.Size(1506, 865);
            this.treeListView1.TabIndex = 0;
            this.treeListView1.UseCompatibleStateImageBehavior = false;
            this.treeListView1.View = System.Windows.Forms.View.Details;
            this.treeListView1.VirtualMode = true;
            // 
            // nameC
            // 
            this.nameC.Text = "Name";
            this.nameC.Width = 252;
            // 
            // versionC
            // 
            this.versionC.Text = "Version";
            this.versionC.Width = 138;
            // 
            // createdC
            // 
            this.createdC.Text = "Created";
            this.createdC.Width = 279;
            // 
            // sizeC
            // 
            this.sizeC.Text = "Size";
            this.sizeC.Width = 152;
            // 
            // FirmwareManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1506, 931);
            this.Controls.Add(this.treeListView1);
            this.Name = "FirmwareManager";
            this.Text = "FirmwareManager";
            this.Load += new System.EventHandler(this.FirmwareManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView treeListView1;
        private BrightIdeasSoftware.OLVColumn nameC;
        private BrightIdeasSoftware.OLVColumn versionC;
        private BrightIdeasSoftware.OLVColumn createdC;
        private BrightIdeasSoftware.OLVColumn sizeC;
    }
}