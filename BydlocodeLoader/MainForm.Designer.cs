﻿namespace BydlocodeLoader
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ipAddressTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.fileNameTxt = new System.Windows.Forms.Label();
            this.selectFile = new System.Windows.Forms.Button();
            this.flashBtn = new System.Windows.Forms.Button();
            this.logTxt = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.HidTxt = new System.Windows.Forms.TextBox();
            this.HIDcheck = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.portTxt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.sendNextBtn = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.interfacePanel = new System.Windows.Forms.Panel();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.portSpeed = new System.Windows.Forms.ComboBox();
            this.ConnectionChecker = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.eSPOTAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.firmwareManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portCombo = new System.Windows.Forms.ComboBox();
            this.tcpConnectBtn = new System.Windows.Forms.Button();
            this.logClearBtn = new System.Windows.Forms.Button();
            this.textToSendTxt = new System.Windows.Forms.TextBox();
            this.btnSendTxt = new System.Windows.Forms.Button();
            this.debugCheck = new System.Windows.Forms.CheckBox();
            this.interfacePanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ipAddressTxt
            // 
            this.ipAddressTxt.Location = new System.Drawing.Point(76, 30);
            this.ipAddressTxt.Name = "ipAddressTxt";
            this.ipAddressTxt.Size = new System.Drawing.Size(100, 20);
            this.ipAddressTxt.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP address";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Файл прошивки (*.hex)|*.hex";
            // 
            // fileNameTxt
            // 
            this.fileNameTxt.AutoSize = true;
            this.fileNameTxt.Location = new System.Drawing.Point(13, 53);
            this.fileNameTxt.Name = "fileNameTxt";
            this.fileNameTxt.Size = new System.Drawing.Size(67, 13);
            this.fileNameTxt.TabIndex = 2;
            this.fileNameTxt.Text = "Not selected";
            // 
            // selectFile
            // 
            this.selectFile.Location = new System.Drawing.Point(16, 69);
            this.selectFile.Name = "selectFile";
            this.selectFile.Size = new System.Drawing.Size(75, 23);
            this.selectFile.TabIndex = 3;
            this.selectFile.Text = "File...";
            this.selectFile.UseVisualStyleBackColor = true;
            this.selectFile.Click += new System.EventHandler(this.selectFile_Click);
            // 
            // flashBtn
            // 
            this.flashBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flashBtn.Location = new System.Drawing.Point(101, 69);
            this.flashBtn.Name = "flashBtn";
            this.flashBtn.Size = new System.Drawing.Size(75, 23);
            this.flashBtn.TabIndex = 4;
            this.flashBtn.Text = "Flash";
            this.flashBtn.UseVisualStyleBackColor = true;
            this.flashBtn.Click += new System.EventHandler(this.flashBtn_Click);
            // 
            // logTxt
            // 
            this.logTxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTxt.Location = new System.Drawing.Point(0, 130);
            this.logTxt.Multiline = true;
            this.logTxt.Name = "logTxt";
            this.logTxt.ReadOnly = true;
            this.logTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTxt.Size = new System.Drawing.Size(871, 320);
            this.logTxt.TabIndex = 5;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(554, 30);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(234, 21);
            this.progressBar1.TabIndex = 6;
            // 
            // HidTxt
            // 
            this.HidTxt.Location = new System.Drawing.Point(182, 69);
            this.HidTxt.Name = "HidTxt";
            this.HidTxt.Size = new System.Drawing.Size(186, 20);
            this.HidTxt.TabIndex = 7;
            // 
            // HIDcheck
            // 
            this.HIDcheck.AutoSize = true;
            this.HIDcheck.Location = new System.Drawing.Point(375, 71);
            this.HIDcheck.Name = "HIDcheck";
            this.HIDcheck.Size = new System.Drawing.Size(67, 17);
            this.HIDcheck.TabIndex = 8;
            this.HIDcheck.Text = "HID filter";
            this.HIDcheck.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = ":";
            // 
            // portTxt
            // 
            this.portTxt.Location = new System.Drawing.Point(189, 30);
            this.portTxt.Name = "portTxt";
            this.portTxt.Size = new System.Drawing.Size(53, 20);
            this.portTxt.TabIndex = 10;
            this.portTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.portTxt_KeyPress);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(632, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 11;
            this.button1.Text = "БЛЯДЬ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // sendNextBtn
            // 
            this.sendNextBtn.Enabled = false;
            this.sendNextBtn.Location = new System.Drawing.Point(713, 59);
            this.sendNextBtn.Name = "sendNextBtn";
            this.sendNextBtn.Size = new System.Drawing.Size(75, 33);
            this.sendNextBtn.TabIndex = 12;
            this.sendNextBtn.Text = "Отправить следующий";
            this.sendNextBtn.UseVisualStyleBackColor = true;
            this.sendNextBtn.Visible = false;
            this.sendNextBtn.Click += new System.EventHandler(this.sendNextBtn_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(3, 10);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(46, 17);
            this.radioButton1.TabIndex = 13;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "TCP";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // interfacePanel
            // 
            this.interfacePanel.Controls.Add(this.radioButton2);
            this.interfacePanel.Controls.Add(this.radioButton1);
            this.interfacePanel.Location = new System.Drawing.Point(448, 59);
            this.interfacePanel.Name = "interfacePanel";
            this.interfacePanel.Size = new System.Drawing.Size(110, 33);
            this.interfacePanel.TabIndex = 14;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(55, 10);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(49, 17);
            this.radioButton2.TabIndex = 14;
            this.radioButton2.Text = "COM";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(355, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "COM:";
            // 
            // portSpeed
            // 
            this.portSpeed.FormattingEnabled = true;
            this.portSpeed.Items.AddRange(new object[] {
            "50",
            "75",
            "110",
            "150",
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "56000",
            "57600",
            "115200"});
            this.portSpeed.Location = new System.Drawing.Point(452, 30);
            this.portSpeed.Name = "portSpeed";
            this.portSpeed.Size = new System.Drawing.Size(97, 21);
            this.portSpeed.TabIndex = 17;
            // 
            // ConnectionChecker
            // 
            this.ConnectionChecker.Interval = 250;
            this.ConnectionChecker.Tick += new System.EventHandler(this.ConnectionChecker_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eSPOTAToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(2, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(793, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // eSPOTAToolStripMenuItem
            // 
            this.eSPOTAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.firmwareManagerToolStripMenuItem});
            this.eSPOTAToolStripMenuItem.Name = "eSPOTAToolStripMenuItem";
            this.eSPOTAToolStripMenuItem.Size = new System.Drawing.Size(63, 22);
            this.eSPOTAToolStripMenuItem.Text = "ESP OTA";
            // 
            // firmwareManagerToolStripMenuItem
            // 
            this.firmwareManagerToolStripMenuItem.Name = "firmwareManagerToolStripMenuItem";
            this.firmwareManagerToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.firmwareManagerToolStripMenuItem.Text = "Firmware manager";
            this.firmwareManagerToolStripMenuItem.Click += new System.EventHandler(this.FirmwareManagerToolStripMenuItem_Click);
            // 
            // portCombo
            // 
            this.portCombo.FormattingEnabled = true;
            this.portCombo.Location = new System.Drawing.Point(387, 30);
            this.portCombo.Name = "portCombo";
            this.portCombo.Size = new System.Drawing.Size(59, 21);
            this.portCombo.TabIndex = 19;
            // 
            // tcpConnectBtn
            // 
            this.tcpConnectBtn.Location = new System.Drawing.Point(248, 30);
            this.tcpConnectBtn.Name = "tcpConnectBtn";
            this.tcpConnectBtn.Size = new System.Drawing.Size(75, 20);
            this.tcpConnectBtn.TabIndex = 20;
            this.tcpConnectBtn.Text = "Connect";
            this.tcpConnectBtn.UseVisualStyleBackColor = true;
            this.tcpConnectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // logClearBtn
            // 
            this.logClearBtn.Location = new System.Drawing.Point(564, 69);
            this.logClearBtn.Name = "logClearBtn";
            this.logClearBtn.Size = new System.Drawing.Size(62, 23);
            this.logClearBtn.TabIndex = 21;
            this.logClearBtn.Text = "Clear log";
            this.logClearBtn.UseVisualStyleBackColor = true;
            this.logClearBtn.Click += new System.EventHandler(this.logClearBtn_Click);
            // 
            // textToSendTxt
            // 
            this.textToSendTxt.Location = new System.Drawing.Point(16, 98);
            this.textToSendTxt.Name = "textToSendTxt";
            this.textToSendTxt.Size = new System.Drawing.Size(385, 20);
            this.textToSendTxt.TabIndex = 22;
            // 
            // btnSendTxt
            // 
            this.btnSendTxt.Enabled = false;
            this.btnSendTxt.Location = new System.Drawing.Point(408, 98);
            this.btnSendTxt.Name = "btnSendTxt";
            this.btnSendTxt.Size = new System.Drawing.Size(75, 23);
            this.btnSendTxt.TabIndex = 23;
            this.btnSendTxt.Text = "Send text";
            this.btnSendTxt.UseVisualStyleBackColor = true;
            this.btnSendTxt.Click += new System.EventHandler(this.btnSendTxt_Click);
            // 
            // debugCheck
            // 
            this.debugCheck.AutoSize = true;
            this.debugCheck.Location = new System.Drawing.Point(489, 101);
            this.debugCheck.Name = "debugCheck";
            this.debugCheck.Size = new System.Drawing.Size(82, 17);
            this.debugCheck.TabIndex = 24;
            this.debugCheck.Text = "Verbose log";
            this.debugCheck.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 450);
            this.Controls.Add(this.debugCheck);
            this.Controls.Add(this.btnSendTxt);
            this.Controls.Add(this.textToSendTxt);
            this.Controls.Add(this.logClearBtn);
            this.Controls.Add(this.tcpConnectBtn);
            this.Controls.Add(this.portCombo);
            this.Controls.Add(this.portSpeed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.interfacePanel);
            this.Controls.Add(this.sendNextBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.portTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HIDcheck);
            this.Controls.Add(this.HidTxt);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.logTxt);
            this.Controls.Add(this.flashBtn);
            this.Controls.Add(this.selectFile);
            this.Controls.Add(this.fileNameTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ipAddressTxt);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Bydlocode flasher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.interfacePanel.ResumeLayout(false);
            this.interfacePanel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ipAddressTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label fileNameTxt;
        private System.Windows.Forms.Button selectFile;
        private System.Windows.Forms.Button flashBtn;
        private System.Windows.Forms.TextBox logTxt;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox HidTxt;
        private System.Windows.Forms.CheckBox HIDcheck;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox portTxt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button sendNextBtn;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Panel interfacePanel;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox portSpeed;
        private System.Windows.Forms.Timer ConnectionChecker;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem eSPOTAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem firmwareManagerToolStripMenuItem;
        private System.Windows.Forms.ComboBox portCombo;
        private System.Windows.Forms.Button tcpConnectBtn;
        private System.Windows.Forms.Button logClearBtn;
        private System.Windows.Forms.TextBox textToSendTxt;
        private System.Windows.Forms.Button btnSendTxt;
        private System.Windows.Forms.CheckBox debugCheck;
    }
}

