﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BydlocodeLoader
{
    public class HostClient
    {
        private static HostClient instance;
        private static object syncRoot = new object();

        public HostClient() { }
        public static HostClient getInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new HostClient();
                }
            }
            return instance;
        }

        /// <summary>
        /// POST запрос
        /// </summary>
        /// <param name="uri">Абсолютная ссылка</param>
        /// <param name="data">Передаваемый объект. Будет сериализирован в xml</param>
        /// <returns>Объект</returns>
        public Tresponse PostRequest<Trequest, Tresponse>(string uri, Trequest data)
            where Trequest : class
            where Tresponse : class
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "POST";
            request.ContentType = "application/xml";
            byte[] bytes = Serialize(data);
            request.ContentLength = bytes.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
            }
            return Request<Tresponse>(request);
        }

        /// <summary>
        /// GET запрос
        /// </summary>
        /// <param name="uri">Абсолютная ссылка с параметрами</param>
        /// <returns>Объект</returns>
        public Tresponse GetRequest<Tresponse>(string uri)
            where Tresponse : class
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";
            request.ContentType = "application/xml";
            return Request<Tresponse>(request);
        }

        private Tresponse Request<Tresponse>(HttpWebRequest request) where Tresponse : class
        {
            Tresponse responseObject = null;
            try
            {
                using (HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        using (MemoryStream memResponseStream = new MemoryStream())
                        {
                            stream.CopyTo(memResponseStream);
                            byte[] responseObj = memResponseStream.ToArray();
                            string response = Encoding.UTF8.GetString(responseObj);
                            Tresponse obj = Deserialize<Tresponse>(responseObj);
                            responseObject = obj;
                        }
                    }
                }
            }
            catch (WebException exception)
            {
                if (exception.Response != null)
                {
                    using (var errorResponse = exception.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            throw new Exception(error);
                        }
                    }
                }
            }
            return responseObject;
        }

        private byte[] Serialize<T>(T obj) where T : class
        {
            if (obj is null) throw new NullReferenceException();
            using (var ms = new MemoryStream())
            {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(ms, obj);
                return ms.ToArray();
            }
        }

        private T Deserialize<T>(byte[] byteArray) where T : class
        {
            string json = Encoding.UTF8.GetString(byteArray);
            T obj = JsonConvert.DeserializeObject<T>(json);
            return obj;
        }
    }
}
